<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Map;
use DB;	
class MapsController extends Controller
{
    public function postMapData(Request $request){
    	$map = new map();
    	$map->latitude = $request->latitude;
    	$map->longitude = $request->longitude;
    	$map->general_category = $request->general_category;
    	$map->public_category = $request->public_category;
    	$map->gender = $request->gender;
    	$map->gender_fecilities = $request->gender_fecilities;
    	$map->disable_fiendly = $request->disable_fiendly;
    	$map->charges =$request->charges;
    	$map->save();

    	return 1;

    }

    public function getMapData()
    {
    	$mapData = DB::table('maps')->get();
    	return json_encode($mapData);
    }
    public function editMapData(Request $request)
    {
    	$map = Map::where("id",$request->id)->first();
    	$map->latitude = $request->latitude;
    	$map->longitude = $request->longitude;
    	$map->general_category = $request->general_category;
    	$map->public_category = $request->public_category;
    	$map->gender = $request->gender;
    	$map->gender_fecilities = $request->gender_fecilities;
    	$map->disable_fiendly = $request->disable_fiendly;
    	$map->charges =$request->charges;
    	$map->save();

		return 1;
    }
}
